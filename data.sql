insert into t_pages(id, title, project_id)
values (1, 'Page One', 1),
       (2, 'Page Two', 1),
       (3, 'Page Three', 2);


insert into t_buttons(id, title, page_id)
values (1, 'Button One', 1),
       (2, 'Button Two', 1),
       (3, 'Button Three', 2),
       (4, 'Button Four', 2),
       (5, 'Button Five', 3);
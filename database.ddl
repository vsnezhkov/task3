create table t_pages
(
    id         bigint not null primary key,
    title      text   not null,
    project_id bigint not null
);

create table t_buttons
(
    id bigint not null primary key,
    title text not null,
    page_id bigint not null,
    project_id bigint
);
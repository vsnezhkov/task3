alter table t_buttons
    add constraint page_fk foreign key (page_id) references t_pages (id);

update t_buttons
set project_id = p.project_id
from t_pages p
where p.id = page_id;